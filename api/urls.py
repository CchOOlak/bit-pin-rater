from django.urls import path, include
from api import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'content', views.ContentViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('rate/<int:ID>/', views.rate_content, name='rate_content'),
]