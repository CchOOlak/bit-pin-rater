import json

from django.http import JsonResponse, HttpResponseNotAllowed, HttpResponseNotFound, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets, mixins

from api.models import *
from api.serializers import *


class ContentViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin, mixins.ListModelMixin):
    queryset = Content.objects.all()
    serializer_class = ContentSerializer


@csrf_exempt
def rate_content(request, ID):
    if request.method == "POST":
        try:
            content = Content.objects.get(pk=ID)
            try:
                parameters = json.loads(request.body)
                username = parameters['username']
                rate = parameters['rate']
            except:
                return HttpResponseBadRequest('invalid parameters')
            
            userRates = ContentUserRate.objects.filter(username=username).filter(content=content)
            if len(userRates) > 0:
                userRates[0].delete()
            userRate = ContentUserRate(
                content=content,
                username=username,
                rate=rate
            )
            userRate.save()
            return JsonResponse({
                "message": f'{username} rate content {ID} by {rate} successfully',
            })
        except:
            return HttpResponseNotFound('content not found')
    else:
        return HttpResponseNotAllowed('method not allowed')
