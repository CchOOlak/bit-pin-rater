from django.db import models
from django.db.models import Avg


class Content(models.Model):
    title = models.CharField(max_length=255)

    @property
    def rate_count(self):
        return len(self.rates.all())
    @property
    def rate_average(self):
        rates = self.rates.all()
        avr = rates.aggregate(Avg('rate'))['rate__avg']
        if avr is None:
            avr = 0.
        return avr
    
    def __str__(self) -> str:
        return self.title



class ContentUserRate(models.Model):
    content = models.ForeignKey(Content, on_delete=models.CASCADE, related_name='rates')
    username = models.CharField(max_length=255)
    rate = models.IntegerField(default=0)