from rest_framework import serializers

from api.models import *

class ContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = ['id', 'title', 'rate_count', 'rate_average']
