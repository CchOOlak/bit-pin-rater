# Bit Pin Rater


## Setup and Run

First you need active your `virtualenv` and install requirements:

    pip install -r requirements.txt

create migrations and migrate:

    python manage.py makemigrations
    python manage.py migrate

Now create a super user for add contents:

    python manage.py createsuperuser

After that run application:

    python manage.py runserver


you can use http://localhost:8000/admin to create your contents.

## API

- Get content
  - method: `GET`
  - path: `/api/content/<contentID>/`
  - response:
    ```jsonc
    {
        "id": <contentId>,
        "title": <titleOfContent>,
        "rate_count": <numberOfUsersThatRate>,
        "rate_average": <averageOfRates>,
    }
    ```
- Rate content
  - method: `POST`
  - path: `/api/rate/<contentID>/`
  - request body:
    ```jsonc
    {
        "username": <nameOfUser>,
        "rate": <rateNumber>,
    } 
    ```